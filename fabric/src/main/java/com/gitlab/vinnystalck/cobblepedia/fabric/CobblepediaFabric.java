package com.gitlab.vinnystalck.cobblepedia.fabric;

import com.gitlab.vinnystalck.cobblepedia.Cobblepedia;

import net.fabricmc.api.ModInitializer;

public class CobblepediaFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        Cobblepedia.init();
    }
}
