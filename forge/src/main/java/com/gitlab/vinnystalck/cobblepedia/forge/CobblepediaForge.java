package com.gitlab.vinnystalck.cobblepedia.forge;

import com.gitlab.vinnystalck.cobblepedia.Cobblepedia;

import net.minecraftforge.fml.common.Mod;

@Mod(Cobblepedia.MOD_ID)
public class CobblepediaForge {

    public CobblepediaForge() {
        Cobblepedia.init();
    }
}
