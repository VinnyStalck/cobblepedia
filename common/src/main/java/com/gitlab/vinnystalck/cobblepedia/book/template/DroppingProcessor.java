package com.gitlab.vinnystalck.cobblepedia.book.template;

import net.minecraft.world.level.Level;
import vazkii.patchouli.api.IComponentProcessor;
import vazkii.patchouli.api.IVariable;
import vazkii.patchouli.api.IVariableProvider;

public class DroppingProcessor implements IComponentProcessor {

    private String pokemon_model_item_id;

    @Override
    public void setup(Level level, IVariableProvider variables) {
        pokemon_model_item_id = ProcessorUtil.model_item_id(variables, "species_id");
    }


    @Override
    public IVariable process(Level level, String key) {
        if (key.equals("pokemon_item")) {
            if (pokemon_model_item_id != null) {
                return IVariable.wrap(pokemon_model_item_id);
            }
        }
        return null;
    } 
}
