# Cobblepedia

# This mod has been moved to [Knowlogy Project](https://gitlab.com/VinnyStalck/knowlogy)

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/mod/cobblepedia)
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/available/curseforge_vector.svg)](https://www.curseforge.com/minecraft/mc-mods/cobblepedia)

An in-game Poképedia wiki book item for [Cobblemon](https://gitlab.com/cable-mc/cobblemon/) using [Patchouli](https://github.com/VazkiiMods/Patchouli/) that shows information from the [official cobblemon wiki](https://wiki.cobblemon.com/index.php/Main_Page) and tracks advancements added by Cobblemon.

## How to get the Poképedia:
- Received by completing the "Choosing a Starter Pokémon" advancement;
- Crafted with a book and any apricorn;
- In creative mode (Found on the Miscellaneous tab);
- or With the command: `/give @s patchouli:guide_book{"patchouli:book": "cobblepedia:cobblepedia"}`.

## Requires
### *Cobblemon* v1.3.1+

[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact-minimal/available/modrinth_vector.svg)](https://modrinth.com/mod/cobblemon) 
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact-minimal/available/curseforge_vector.svg)](https://curseforge.com/minecraft/mc-mods/cobblemon)

### *Patchouli* 
[![Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact-minimal/available/modrinth_vector.svg)](https://modrinth.com/mod/patchouli) 
[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact-minimal/available/curseforge_vector.svg)](https://curseforge.com/minecraft/mc-mods/patchouli)[![CurseForge](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact-minimal/requires/fabric-api_vector.svg)](https://curseforge.com/minecraft/mc-mods/patchouli-fabric)

## Other info
Texture based on [Torrezx-Improved Library](https://www.curseforge.com/minecraft/texture-packs/torrezx-improved-library) books and inspired by the books in [Legends of Arceus trailer](https://youtu.be/SbIA8FKhwl0) intro books

### Discussion on the Official Cobblemon Discord Server:

[![Discord](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3.1.0/assets/compact/social/discord-singular_vector.svg)](https://discord.com/channels/934267676354834442/1091579923396820992)
